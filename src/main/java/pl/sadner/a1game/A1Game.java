package pl.sadner.a1game;

import pl.sadner.a1game.infrastructure.context.ApplicationContext;

public class A1Game {
    public static void main(String[] args) {

        var engine = new GameEngine(new ApplicationContext());
        engine.start();
    }
}
