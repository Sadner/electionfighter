package pl.sadner.a1game;

import pl.sadner.a1game.commons.CharacterClass;
import pl.sadner.a1game.commons.Millisecond;
import pl.sadner.a1game.components.*;
import pl.sadner.a1game.components.utils.Command;
import pl.sadner.a1game.components.utils.Display;
import pl.sadner.a1game.game.application.port.in.CreateCharacterCommand;
import pl.sadner.a1game.game.application.port.in.GameStatusDTO;
import pl.sadner.a1game.infrastructure.Result;
import pl.sadner.a1game.infrastructure.context.ApplicationContext;
import pl.sadner.a1game.printables.Color;
import pl.sadner.a1game.printables.Printer;

import java.text.MessageFormat;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

import static pl.sadner.a1game.config.GameConfig.getInt;
import static pl.sadner.a1game.config.GameConfig.getText;
import static pl.sadner.a1game.printables.Printer.print;
import static pl.sadner.a1game.printables.Printer.printWithColor;

public class GameEngine {
    private final ApplicationContext applicationContext;
    private final BattleComponent battleComponent;
    private final ExplorationComponent explorationComponent;

    public GameEngine(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.battleComponent = new BattleComponent(this.applicationContext);
        this.explorationComponent = new ExplorationComponent(this.applicationContext);
    }

    public void start() {
        Display.clear();
        new TextImage(getText("game.name"),
                getInt("game.window.width"),
                getInt("game.window.height"))
                .print();
        waitFor(new Millisecond(2000));
        Display.clear();
        Menu<Runnable> menu = new Menu.MenuBuilder<Runnable>()
                .title(getText("game.menu.mainMenu"))
                .color(Color.ANSI_PURPLE)
                .withOption(getText("game.mainMenu.newGame"), this::createNewGame, Command.OPTION_1)
                .withOption(getText("game.mainMenu.loadGame"), this::loadAndPlay, Command.OPTION_2)
                .build();

        menu.print();
        menu.getSelectedOption().orElseThrow().getValue().run();

        while (true) {
            getGameStatus().ifSuccess(status -> {
                switch (status.getState()) {
                    case WAITING_FOR_MOVE -> explorationComponent.explore();
                    case BATTLE_IN_PROGRESS -> battleComponent.battle(status.getState());
                }
            });
        }
    }

    private void loadAndPlay() {
        applicationContext.getLoadGameUseCase().loadGame();
        getGameStatus().fold(status -> {
            switch (status.getState()) {
                case WAITING_FOR_MOVE -> explorationComponent.explore();
                case BATTLE_IN_PROGRESS -> {
                    applicationContext.getLoadBattleUseCase().loadBattle();
                    battleComponent.battle(status.getState());
                }
            }
        }, throwable -> {
            printWithColor("Somthing went wrong. Could not load the game. Please start new.", Color.ANSI_RED);
            createNewGame();
        });
    }

    private void createNewGame() {
        applicationContext.getCreateGameUseCase().createGame();
        print("Your name: ");
        String name = getPlayerInput();
        List<CharacterClass> availableClasses = applicationContext
                .getGetAvailableClassesUseCase()
                .getAvailableClasses();

        Menu<CharacterClass> classMenu = new Menu.MenuBuilder<CharacterClass>()
                .title("Chose your class: ")
                .color(Color.ANSI_PURPLE)
                .withOptions(availableClasses.stream().collect(Collectors.toMap(Function.identity(), CharacterClass::getName)))
                .build();
        classMenu.print();
        Option<CharacterClass> characterClassOption = classMenu.getSelectedOption().orElseThrow();

        applicationContext
                .getCreateCharacterUseCase()
                .createCharacter(new CreateCharacterCommand(name, characterClassOption.getValue()));
        Display.clear();
        Printer.print(MessageFormat.format(getText("game.welcomeMessage"), name, characterClassOption.getValue().getName()));
        applicationContext.getExploreUseCase().explore();
    }

    private void waitFor(Millisecond millis) {
        try {
            Thread.sleep(millis.getValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String getPlayerInput() {
        var scanner = new Scanner(System.in);
        return scanner.next().trim();
    }

    private Result<GameStatusDTO, Throwable> getGameStatus() {
        return applicationContext.getGetGameStatusUseCase().getGameStatus();
    }

}
