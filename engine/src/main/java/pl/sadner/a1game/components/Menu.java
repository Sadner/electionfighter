package pl.sadner.a1game.components;

import pl.sadner.a1game.components.utils.Command;
import pl.sadner.a1game.components.utils.Display;
import pl.sadner.a1game.printables.Color;
import pl.sadner.a1game.printables.Printable;
import pl.sadner.a1game.printables.Printer;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Menu<T> implements Printable {
    private final List<Option<T>> options;
    private final String title;

    public Menu(List<Option<T>> options, String title) {
        this.options = options;
        this.title = title;
    }

    public void print() {
        Printer.print(title);
        options.forEach(Option::print);
    }

    public Optional<Option<T>> getSelectedOption() {
        Command playerCommand = Display.getPlayerCommand(
                options.stream().map(Option::getCommand).collect(Collectors.toUnmodifiableList())
        );
        return options.stream()
                .filter(option -> option.getCommand().equals(playerCommand))
                .findAny();
    }

    public static class MenuBuilder<T> {
        private final List<OptionEntry> options = new ArrayList<>();
        private String title;
        private Color color;

        public MenuBuilder<T> withQuitSave() {
            this.options.add(new OptionEntry("Save", null, Command.SAVE));
            this.options.add(new OptionEntry("Quit", null, Command.QUIT));
            return this;
        }

        private class OptionEntry {
            private final String label;
            private final T value;
            private final Command command;

            public OptionEntry(String label, T value, Command command) {
                this.label = label;
                this.value = value;
                this.command = command;
            }

            public Option<T> toOption() {
                return new Option<>(label, value, command, color != null ? color : Color.ANSI_WHITE);
            }
        }

        public MenuBuilder<T> title(String title) {
            this.title = title;
            return this;
        }

        public MenuBuilder<T> color(Color color) {
            this.color = color;
            return this;
        }

        public MenuBuilder<T> withOption(String label, T value, Command command) {
            options.add(new OptionEntry(label, value, command));
            return this;
        }

        public MenuBuilder<T> withOptions(Map<T, String> values) {
            var valuesEntries = new ArrayList<>(values.entrySet());
            List<Command> optionCommands = Arrays.stream(Command.values())
                    .filter(Command::isOption)
                    .collect(Collectors.toList());
            IntStream.range(0, optionCommands.size())
                    .forEach(idx -> options.add(
                            new OptionEntry(valuesEntries.get(idx).getValue(),
                                    valuesEntries.get(idx).getKey(),
                                    optionCommands.get(idx))
                            )
                    );
            return this;
        }

        public Menu<T> build() {
            return new Menu<T>(
                    options.stream()
                            .map(OptionEntry::toOption).collect(Collectors.toList()),
                    title != null ? title : ""
            );
        }
    }
}
