package pl.sadner.a1game.components.utils;

import static pl.sadner.a1game.config.GameConfig.*;

public enum Command {
    OPTION_1("1", true),
    OPTION_2("2", true),
    QUIT(getText("command.quit.label")),
    SAVE(getText("command.save.label")),
    NORTH(getText("command.north.label")),
    SOUTH(getText("command.south.label")),
    EAST(getText("command.east.label")),
    WEST(getText("command.west.label"));

    private final String pattern;
    private final boolean isOption;

    Command(String pattern) {
        this.pattern = pattern.toUpperCase();
        this.isOption = false;
    }

    Command(String pattern, boolean isOption) {
        this.pattern = pattern;
        this.isOption = isOption;
    }

    public String getPattern() {
        return pattern;
    }

    public boolean isOption() {
        return isOption;
    }

    public boolean match(String command) {
        return this.pattern.equals(command.toUpperCase().trim());
    }
}
