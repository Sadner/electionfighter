package pl.sadner.a1game.components;

import pl.sadner.a1game.battle.application.port.in.AttackCommand;
import pl.sadner.a1game.battle.domain.BattleStatus;
import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.components.utils.Display;
import pl.sadner.a1game.game.application.port.in.GameStatusDTO;
import pl.sadner.a1game.game.domain.GameState;
import pl.sadner.a1game.infrastructure.Result;
import pl.sadner.a1game.infrastructure.context.ApplicationContext;
import pl.sadner.a1game.printables.Color;
import pl.sadner.a1game.printables.Printer;

import java.util.function.Function;
import java.util.stream.Collectors;

import static pl.sadner.a1game.components.utils.Display.clear;
import static pl.sadner.a1game.config.GameConfig.getText;

public class BattleComponent {
    private final ApplicationContext applicationContext;

    public BattleComponent(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void battle(GameState state) {
        clear();
        Printer.printWithColor(getText("game.battle.start"), Color.ANSI_RED);
        var gameStatus = state;
        do {
            BattleStatus battleStatus = applicationContext.getGetBattleStatusUseCase().getBattleStatus();
            printHealth(battleStatus);
            var menu = prepareMenu(battleStatus);
            menu.print();
            Option<Attack> selectedOption = menu.getSelectedOption().orElseThrow();
            switch (selectedOption.getCommand()) {
                case OPTION_1, OPTION_2 -> performAttack(selectedOption.getValue());
                default -> Display.handleCommand(selectedOption.getCommand(), applicationContext);
            }

            try {
                gameStatus = getGameStatus().getOrThrow().getState();
            } catch (Throwable throwable) {
                break;
            }
        } while (gameStatus == GameState.BATTLE_IN_PROGRESS);
        Printer.printWithColor(getText("game.battle.won"), Color.ANSI_GREEN);
        getGameStatus().ifSuccess(status -> {
            Printer.printWithColor("Your experience: " + status.getCharacterExperience().getValue(), Color.ANSI_CYAN);
        });

    }

    private void printHealth(BattleStatus battleStatus) {
        var hpLabel = getText("game.battle.hp.label");
        Printer.printWithColor("Your " + hpLabel + ": " + battleStatus.getPlayerHealth().getValue(), Color.ANSI_GREEN);
        Printer.printWithColor("Enemy " + hpLabel + ": " + battleStatus.getEnemyHealth().getValue(), Color.ANSI_RED);
    }

    private Menu<Attack> prepareMenu(BattleStatus battleStatus) {
        return new Menu.MenuBuilder<Attack>()
                .color(Color.ANSI_YELLOW)
                .title("Attacks: ")
                .withOptions(battleStatus.getAvailableAttack()
                        .stream()
                        .collect(
                                Collectors.toMap(Function.identity(),
                                        attack -> attack.getName() + "(" + attack.getBaseDamage() + ")")
                        )
                )
                .withQuitSave()
                .build();
    }

    private void performAttack(Attack value) {
        applicationContext.getPerformAttackUseCase().performAttack(new AttackCommand(value));
    }

    private Result<GameStatusDTO, Throwable> getGameStatus() {
        return applicationContext.getGetGameStatusUseCase().getGameStatus();
    }
}
