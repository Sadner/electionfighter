package pl.sadner.a1game.components;

import pl.sadner.a1game.printables.Color;
import pl.sadner.a1game.printables.Printable;
import pl.sadner.a1game.printables.Printer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TextImage implements Printable {
    public static final int TOP_OFFSET = 10;
    private final String text;
    private final int width;
    private final int height;
    private final int startX;
    private final int startY;

    public TextImage(String text, int width, int height, int startX, int startY) {
        this.text = text;
        this.width = width;
        this.height = height;
        this.startX = startX;
        this.startY = startY + TOP_OFFSET;
    }

    public TextImage(String text, int width, int height) {
        this.text = prepareText(text);
        this.width = width;
        this.height = height;
        this.startX = 1;
        this.startY = TOP_OFFSET;
    }

    private String prepareText(String text) {
        return text.chars()
                .map(Character::toUpperCase)
                .collect(StringBuilder::new, (stringBuilder, value) -> stringBuilder.appendCodePoint(value).append(" "), StringBuilder::append)
                .toString();
    }

    public void print() {
        BufferedImage bufferedImage = createBufferedImage();
        drawString(bufferedImage);
        String renderedText = fillImageWithTextChars(bufferedImage);
        Printer.printWithColor(renderedText, Color.ANSI_BLUE);
    }

    private String fillImageWithTextChars(BufferedImage bufferedImage) {
        return IntStream.range(0, height)
                .mapToObj(y -> IntStream.range(0, width)
                        .mapToObj(x -> getCharForRGBValue(bufferedImage.getRGB(x, y)))
                        .collect(Collectors.joining()).concat("\n"))
                .collect(Collectors.joining());
    }

    private void drawString(BufferedImage bufferedImage) {
        Graphics graphics = bufferedImage.getGraphics();
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics2D.drawString(text, startX, startY);
    }

    private BufferedImage createBufferedImage() {
        return new BufferedImage(
                width, height,
                BufferedImage.TYPE_INT_RGB);
    }

    private String getCharForRGBValue(int rgb) {
        return rgb == -16777216 ? " " : "%";
    }
}
