package pl.sadner.a1game.components.utils;

import pl.sadner.a1game.infrastructure.context.ApplicationContext;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Display {
    public static void clear() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Command getPlayerCommand(List<Command> availableCommands) {
        var scanner = new Scanner(System.in);
        var input = scanner.next().toUpperCase().trim();
        var maybeCommand = matchCommand(input, availableCommands);
        while (maybeCommand.isEmpty()) {
            System.out.println("Unknown command. Available commands: " + availableCommands
                    .stream()
                    .map(Command::getPattern)
                    .collect(Collectors.joining(" ")));
            input = scanner.next().toUpperCase().trim();
            maybeCommand = matchCommand(input, availableCommands);
        }
        return maybeCommand.get();
    }

    public static void exit() {
        System.exit(0);
    }

    private static Optional<Command> matchCommand(String input, List<Command> availableCommands) {
        return availableCommands.stream()
                .filter(command -> command.match(input))
                .findAny();
    }

    public static void handleCommand(Command command, ApplicationContext applicationContext) {
        switch (command) {
            case QUIT -> Display.exit();
            case SAVE -> {
                applicationContext.getSaveGameUseCase().saveGame();
                applicationContext.getSaveBattleUseCase().saveBattle();
            }
        }
    }
}

