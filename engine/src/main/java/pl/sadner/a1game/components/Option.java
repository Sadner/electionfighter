package pl.sadner.a1game.components;

import pl.sadner.a1game.components.utils.Command;
import pl.sadner.a1game.printables.Color;
import pl.sadner.a1game.printables.Printable;
import pl.sadner.a1game.printables.Printer;

public class Option<T> implements Printable {
    private final String label;
    private final T value;
    private final Command command;
    private final Color color;

    public Option(String label, T value, Command command, Color color) {
        this.label = label;
        this.value = value;
        this.command = command;
        this.color = color;
    }

    public String getLabel() {
        return label;
    }

    public T getValue() {
        return value;
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public void print() {
        Printer.printWithColor(command.getPattern() + " -> " + label, color);
    }
}
