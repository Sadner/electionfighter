package pl.sadner.a1game.components;

import pl.sadner.a1game.components.utils.Command;
import pl.sadner.a1game.components.utils.Display;
import pl.sadner.a1game.game.application.port.in.GameStatusDTO;
import pl.sadner.a1game.game.application.port.in.MoveCommand;
import pl.sadner.a1game.game.domain.Direction;
import pl.sadner.a1game.game.domain.EndOfMapException;
import pl.sadner.a1game.game.domain.GameState;
import pl.sadner.a1game.infrastructure.Result;
import pl.sadner.a1game.infrastructure.context.ApplicationContext;
import pl.sadner.a1game.printables.Color;
import pl.sadner.a1game.printables.Printer;

import static pl.sadner.a1game.config.GameConfig.getText;
import static pl.sadner.a1game.game.domain.GameState.WAITING_FOR_MOVE;
import static pl.sadner.a1game.printables.Printer.print;

public class ExplorationComponent {

    private final ApplicationContext applicationContext;

    public ExplorationComponent(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void explore() {
        Printer.printWithColor(getText("game.explore.start"), Color.ANSI_GREEN);
        Menu<Direction> directionMenu = new Menu.MenuBuilder<Direction>()
                .color(Color.ANSI_GREEN)
                .withOption(Command.EAST.name(), Direction.EAST, Command.EAST)
                .withOption(Command.WEST.name(), Direction.WEST, Command.WEST)
                .withOption(Command.NORTH.name(), Direction.NORTH, Command.NORTH)
                .withOption(Command.SOUTH.name(), Direction.SOUTH, Command.SOUTH)
                .withQuitSave()
                .build();
        directionMenu.print();
        dispatchMoveCommand(directionMenu.getSelectedOption().orElseThrow().getCommand());

        GameState gameStatus;
        try {
            gameStatus = getGameStatus().getOrThrow().getState();
        } catch (Throwable throwable) {
            return;
        }
        while (gameStatus == WAITING_FOR_MOVE) {
            Printer.printWithColor(getText("game.explore.move.empty"), Color.ANSI_PURPLE);
            directionMenu.print();
            dispatchMoveCommand(directionMenu.getSelectedOption().orElseThrow().getCommand());
            try {
                gameStatus = getGameStatus().getOrThrow().getState();
            } catch (Throwable throwable) {
                return;
            }
        }
    }

    private void dispatchMoveCommand(Command playerCommand) {
        switch (playerCommand) {
            case WEST, NORTH, EAST, SOUTH -> move(playerCommand);
            default -> Display.handleCommand(playerCommand, applicationContext);
        }
    }

    private void move(Command playerCommand) {
        var direction = Direction.valueOf(playerCommand.name());
        var result = applicationContext.getExploreUseCase().move(new MoveCommand(direction, 1L));
        result.fold(
                success -> print(getText("game.explore.move.label") + direction.name()),
                failure -> {
                    if (failure instanceof EndOfMapException) {
                        print(getText("game.explore.move.endOfMap"));
                    }
                });
    }

    private Result<GameStatusDTO, Throwable> getGameStatus() {
        return applicationContext.getGetGameStatusUseCase().getGameStatus();
    }
}
