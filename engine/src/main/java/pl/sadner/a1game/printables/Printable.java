package pl.sadner.a1game.printables;

public interface Printable {
 void print();
}
