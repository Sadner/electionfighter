package pl.sadner.a1game.printables;

public class Printer {
    public static final String ANSI_RESET = "\u001B[0m";

    public static void printWithColor(Object content, Color color) {
        System.out.println(color.getColorCode() + content.toString() + ANSI_RESET);
    }
    public static void print(Object content) {
        System.out.println(content.toString());
    }
}
