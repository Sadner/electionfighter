package pl.sadner.a1game.battle.adapter.out;

import pl.sadner.a1game.battle.application.port.out.BattleLoadPort;
import pl.sadner.a1game.battle.application.port.out.BattleSavePort;
import pl.sadner.a1game.battle.domain.Battle;
import pl.sadner.a1game.persistance.Repository;

public class BattleRepository extends Repository<Battle> implements BattleSavePort, BattleLoadPort {
    @Override
    protected String getFileName() {
        return "battle.txt";
    }
}
