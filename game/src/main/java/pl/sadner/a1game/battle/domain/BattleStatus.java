package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Health;

import java.util.List;

public class BattleStatus {
    private final Health playerHealth;
    private final List<Attack> availableAttack;
    private final Health enemyHealth;

    public BattleStatus(Health playerHealth, List<Attack> availableAttack, Health enemyHealth) {
        this.playerHealth = playerHealth;
        this.availableAttack = availableAttack;
        this.enemyHealth = enemyHealth;
    }

    public Health getPlayerHealth() {
        return playerHealth;
    }

    public Health getEnemyHealth() {
        return enemyHealth;
    }

    public List<Attack> getAvailableAttack() {
        return availableAttack;
    }
}
