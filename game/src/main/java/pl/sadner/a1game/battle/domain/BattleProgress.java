package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

 class BattleProgress implements Serializable {
    private final List<Action> playerActions = new ArrayList<>();
    private final List<Action> enemyActions = new ArrayList<>();

     List<Action> getPlayerActions() {
        return playerActions;
    }

     List<Action> getEnemyActions() {
        return enemyActions;
    }
}
