package pl.sadner.a1game.battle.application.port.in;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Health;

import java.util.List;

public class StartBattleCommand {
    private final long characterId;
    private final long enemyId;
    private final Health playerHealth;
    private final List<Attack> availableAttacks;

    public StartBattleCommand(long characterId, long enemyId, Health playerHealth, List<Attack> availableAttacks) {
        this.characterId = characterId;
        this.enemyId = enemyId;
        this.playerHealth = playerHealth;
        this.availableAttacks = availableAttacks;
    }

    public long getCharacterId() {
        return characterId;
    }

    public long getEnemyId() {
        return enemyId;
    }

    public Health getPlayerHealth() {
        return playerHealth;
    }

    public List<Attack> getAvailableAttacks() {
        return availableAttacks;
    }
}
