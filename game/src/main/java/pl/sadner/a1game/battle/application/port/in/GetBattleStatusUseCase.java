package pl.sadner.a1game.battle.application.port.in;

import pl.sadner.a1game.battle.domain.BattleStatus;

public interface GetBattleStatusUseCase {
    BattleStatus getBattleStatus();
}
