package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.infrastructure.eventbus.Event;

public class BattleWonEvent extends Event {
    private final Experience experience;

    public BattleWonEvent(Experience experience) {
        this.experience = experience;
    }

    public Experience getExperience() {
        return experience;
    }
}
