package pl.sadner.a1game.battle.application.port.in;

import pl.sadner.a1game.commons.Attack;

public class AttackCommand {
    private final Attack performedAttack;

    public AttackCommand(Attack performedAttack) {
        this.performedAttack = performedAttack;
    }

    public Attack getPerformedAttack() {
        return performedAttack;
    }
}
