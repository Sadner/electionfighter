package pl.sadner.a1game.battle.domain;


import pl.sadner.a1game.battle.application.port.in.StartBattleCommand;
import pl.sadner.a1game.commons.Health;
import pl.sadner.a1game.config.GameConfig;


public class DefaultBattleFactory implements BattleFactory {

    @Override
    public Battle create(StartBattleCommand startBattleCommand) {
        var playerHealth = startBattleCommand.getPlayerHealth();
        var characterStats = new CharacterStats(startBattleCommand.getCharacterId(), playerHealth, startBattleCommand.getAvailableAttacks());
        var enemyStats = new EnemyInfo(startBattleCommand.getEnemyId(), Health.of(GameConfig.getLong("enemy.health")), new SimpleAttack());

        return new Battle(characterStats, enemyStats, new BattleProgress());
    }
}
