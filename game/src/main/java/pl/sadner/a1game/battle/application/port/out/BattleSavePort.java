package pl.sadner.a1game.battle.application.port.out;

import pl.sadner.a1game.battle.domain.Battle;

public interface BattleSavePort {
    void save(Battle battle);
}
