package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Health;

import java.io.Serializable;
import java.util.List;

 class CharacterStats implements Serializable {
    private final long characterId;
    private final Health health;
    private final List<Attack> availableAttacks;

     CharacterStats(long characterId, Health health, List<Attack> availableAttacks) {
        this.characterId = characterId;
        this.health = health;
        this.availableAttacks = availableAttacks;
    }

     long getCharacterId() {
        return characterId;
    }

     Health getHealth() {
        return health;
    }

     List<Attack> getAvailableAttacks() {
        return availableAttacks;
    }
}
