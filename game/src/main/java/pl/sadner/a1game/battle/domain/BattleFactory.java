package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.battle.application.port.in.StartBattleCommand;

public interface BattleFactory {

    Battle create(StartBattleCommand startBattleCommand);
}
