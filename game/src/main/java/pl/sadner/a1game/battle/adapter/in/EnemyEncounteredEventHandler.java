package pl.sadner.a1game.battle.adapter.in;

import pl.sadner.a1game.battle.application.port.in.StartBattleCommand;
import pl.sadner.a1game.battle.application.port.in.StartBattleUseCase;
import pl.sadner.a1game.game.domain.EnemyEncounteredEvent;
import pl.sadner.a1game.infrastructure.eventbus.Event;
import pl.sadner.a1game.infrastructure.eventbus.EventConsumer;
import pl.sadner.a1game.infrastructure.eventbus.Topic;

public class EnemyEncounteredEventHandler extends EventConsumer {

    private final StartBattleUseCase startBattleUseCase;

    public EnemyEncounteredEventHandler(StartBattleUseCase startBattleUseCase) {
        this.startBattleUseCase = startBattleUseCase;
        subscribe(Topic.of("pl/sadner/a1game/game"));
    }

    @Override
    public void handle(Event event) {
        if (!(event instanceof EnemyEncounteredEvent))
            return;
        var enemyEncounteredEvent = ((EnemyEncounteredEvent) event);
        StartBattleCommand startBattleCommand = new StartBattleCommand(enemyEncounteredEvent.getCharacterId(),
                enemyEncounteredEvent.getEnemyId(),
                enemyEncounteredEvent.getPlayerHealth(),
                enemyEncounteredEvent.getAvailableAttacks());
        startBattleUseCase.startBattle(startBattleCommand);
    }
}
