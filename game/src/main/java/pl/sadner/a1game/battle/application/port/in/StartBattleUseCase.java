package pl.sadner.a1game.battle.application.port.in;

public interface StartBattleUseCase {
    void startBattle(StartBattleCommand startBattleCommand);
}
