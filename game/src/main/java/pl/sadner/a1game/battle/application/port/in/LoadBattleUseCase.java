package pl.sadner.a1game.battle.application.port.in;

public interface LoadBattleUseCase {
    void loadBattle();
}
