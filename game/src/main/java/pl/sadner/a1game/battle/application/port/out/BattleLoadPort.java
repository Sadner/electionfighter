package pl.sadner.a1game.battle.application.port.out;

import pl.sadner.a1game.battle.domain.Battle;

import java.util.Optional;

public interface BattleLoadPort {
    Optional<Battle> load();
}
