package pl.sadner.a1game.battle.application;

import pl.sadner.a1game.battle.application.port.in.*;
import pl.sadner.a1game.battle.application.port.out.BattleLoadPort;
import pl.sadner.a1game.battle.application.port.out.BattleSavePort;
import pl.sadner.a1game.battle.domain.Battle;
import pl.sadner.a1game.battle.domain.BattleFactory;
import pl.sadner.a1game.battle.domain.BattleStatus;
import pl.sadner.a1game.infrastructure.eventbus.EventPublisher;
import pl.sadner.a1game.infrastructure.eventbus.Topic;

import java.util.Objects;

public class BattleService implements StartBattleUseCase,
        PerformAttackUseCase,
        GetBattleStatusUseCase,
        LoadBattleUseCase,
        SaveBattleUseCase {

    private Battle battle;
    private final BattleFactory battleFactory;
    private final EventPublisher eventPublisher;
    private final BattleLoadPort battleLoadPort;
    private final BattleSavePort battleSavePort;

    public BattleService(BattleFactory battleFactory,
                         EventPublisher eventPublisher,
                         BattleLoadPort battleLoadPort,
                         BattleSavePort battleSavePort) {
        this.battleFactory = battleFactory;
        this.eventPublisher = eventPublisher;
        this.battleLoadPort = battleLoadPort;
        this.battleSavePort = battleSavePort;
    }

    @Override
    public void startBattle(StartBattleCommand startBattleCommand) {
        this.battle = battleFactory.create(startBattleCommand);
    }

    @Override
    public void performAttack(AttackCommand attackCommand) {
        Objects.requireNonNull(battle);
        battle.performAttack(attackCommand.getPerformedAttack());
        battle.getEmittedEvents().forEach(event -> eventPublisher.publish(Topic.of("pl/sadner/a1game/battle"), event));
    }

    @Override
    public BattleStatus getBattleStatus() {
        Objects.requireNonNull(battle);
        return battle.getBattleStatus();
    }

    @Override
    public void saveBattle() {
        if (battle != null) {
            battleSavePort.save(this.battle);
        }
    }

    @Override
    public void loadBattle() {
        this.battle = battleLoadPort.load().orElseThrow();
    }
}
