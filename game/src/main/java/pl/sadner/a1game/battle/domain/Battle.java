package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Action;
import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.commons.Health;
import pl.sadner.a1game.infrastructure.eventbus.Event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Battle implements Serializable {
    private final CharacterStats characterStats;
    private final EnemyInfo enemyInfo;
    private final BattleProgress battleProgress;
    private final List<Event> emittedEvents = new ArrayList<>();

    public Battle(CharacterStats characterStats, EnemyInfo enemyInfo, BattleProgress battleProgress) {
        this.characterStats = characterStats;
        this.enemyInfo = enemyInfo;
        this.battleProgress = battleProgress;
    }

    public BattleStatus getBattleStatus() {
        var playerHealth = Health.of(characterStats.getHealth().getValue() - sumAttackDamage(battleProgress.getEnemyActions()));
        var enemyHealth = Health.of(enemyInfo.getHealth().getValue() - sumAttackDamage(battleProgress.getPlayerActions()));
        return new BattleStatus(playerHealth, Collections.unmodifiableList(characterStats.getAvailableAttacks()), enemyHealth);
    }

    private Long sumAttackDamage(List<Action> actions) {
        return actions
                .stream()
                .filter(Attack.class::isInstance)
                .map(Attack.class::cast)
                .map(Attack::getBaseDamage)
                .reduce(0L, Long::sum);
    }

    public void performAttack(Attack attack) {
        this.battleProgress.getPlayerActions().add(attack);
        this.battleProgress.getEnemyActions().add(enemyInfo.getAttack());

        if (this.getBattleStatus().getEnemyHealth().getValue() <= 0) {
            emittedEvents.add(new BattleWonEvent(new Experience(enemyInfo.getHealth().getValue())));
        }
    }

    public List<Event> getEmittedEvents() {
        return emittedEvents;
    }
}
