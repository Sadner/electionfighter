package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.config.GameConfig;

 public class SimpleAttack extends Attack {
     public SimpleAttack() {
        super(GameConfig.getText("attack.simple.name"), GameConfig.getLong("attack.simple.damage"));
    }
}
