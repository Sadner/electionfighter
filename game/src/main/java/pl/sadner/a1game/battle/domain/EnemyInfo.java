package pl.sadner.a1game.battle.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Health;

import java.io.Serializable;

 class EnemyInfo implements Serializable {
    private final long enemyId;
    private final Health health;
    private final Attack attack;

     EnemyInfo(long enemyId, Health health, Attack attack) {
        this.enemyId = enemyId;
        this.health = health;
        this.attack = attack;
    }

     long getEnemyId() {
        return enemyId;
    }

     Health getHealth() {
        return health;
    }

     Attack getAttack() {
        return attack;
    }
}
