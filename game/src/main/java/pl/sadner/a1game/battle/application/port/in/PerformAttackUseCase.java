package pl.sadner.a1game.battle.application.port.in;

public interface PerformAttackUseCase {
    void performAttack(AttackCommand attackCommand);
}
