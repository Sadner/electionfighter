package pl.sadner.a1game.infrastructure.context;

import pl.sadner.a1game.battle.adapter.in.EnemyEncounteredEventHandler;
import pl.sadner.a1game.battle.adapter.out.BattleRepository;
import pl.sadner.a1game.battle.application.BattleService;
import pl.sadner.a1game.battle.application.port.in.*;
import pl.sadner.a1game.battle.application.port.out.BattleLoadPort;
import pl.sadner.a1game.battle.application.port.out.BattleSavePort;
import pl.sadner.a1game.battle.domain.BattleFactory;
import pl.sadner.a1game.battle.domain.DefaultBattleFactory;
import pl.sadner.a1game.game.adapter.in.BattleEventHandler;
import pl.sadner.a1game.game.adapter.out.EmmitGameEventAdapter;
import pl.sadner.a1game.game.adapter.out.GameRepository;
import pl.sadner.a1game.game.application.GameService;
import pl.sadner.a1game.game.application.port.in.*;
import pl.sadner.a1game.game.application.port.out.EmmitGameEventPort;
import pl.sadner.a1game.game.application.port.out.LoadGamePort;
import pl.sadner.a1game.game.application.port.out.SaveGamePort;
import pl.sadner.a1game.game.domain.DefaultGameFactory;
import pl.sadner.a1game.game.domain.GameFactory;
import pl.sadner.a1game.infrastructure.eventbus.EventPublisher;

public class ApplicationContext {
    private EventPublisher eventPublisher;
    private EmmitGameEventAdapter emmitGameEventAdapter;
    private GameService gameService;
    private GameFactory gameFactory;
    private GameRepository gameRepository;
    private StartBattleUseCase startBattleUseCase;

    private BattleFactory battleFactory;
    private BattleRepository battleRepository;
    private BattleService battleService;

    private final EnemyEncounteredEventHandler enemyEncounteredEventHandler = new EnemyEncounteredEventHandler(getStartBattleUseCase());
    private final BattleEventHandler battleEventHandler = new BattleEventHandler(getExploreUseCase(), getAwardPlayerUseCase());


    public StartBattleUseCase getStartBattleUseCase() {
        if (this.startBattleUseCase == null) {
            this.startBattleUseCase = getBattleService();
        }
        return startBattleUseCase;
    }

    private BattleService getBattleService() {
        if (this.battleService == null) {
            this.battleService = new BattleService(getBattleFactory(), getEventPublisher(), getBattleRepository(), getBattleRepository());
        }
        return battleService;
    }

    public BattleRepository getBattleRepository() {
        if (this.battleRepository == null) {
            this.battleRepository = new BattleRepository();
        }
        return battleRepository;
    }

    public PerformAttackUseCase getPerformAttackUseCase() {
        return getBattleService();
    }

    public GetBattleStatusUseCase getGetBattleStatusUseCase() {
        return getBattleService();
    }

    public SaveBattleUseCase getSaveBattleUseCase() {
        return getBattleService();
    }

    public LoadBattleUseCase getLoadBattleUseCase() {
        return getBattleService();
    }

    public BattleFactory getBattleFactory() {
        if (this.battleFactory == null) {
            this.battleFactory = new DefaultBattleFactory();
        }
        return battleFactory;
    }

    public BattleLoadPort getBattleLoadPort() {
        return getBattleRepository();
    }

    public BattleSavePort getBattleSavePort() {
        return getBattleRepository();
    }

    public GetGameStatusUseCase getGameStatusUseCase() {
        return getGameService();
    }

    public AwardPlayerUseCase getAwardPlayerUseCase() {
        return getGameService();
    }

    public ExploreUseCase getExploreUseCase() {
        return getGameService();
    }

    public LoadGameUseCase getLoadGameUseCase() {
        return getGameService();
    }

    public CreateCharacterUseCase getCreateCharacterUseCase() {
        return getGameService();
    }

    public CreateGameUseCase getCreateGameUseCase() {
        return getGameService();
    }

    public GetAvailableClassesUseCase getGetAvailableClassesUseCase() {
        return getGameService();
    }

    public SaveGameUseCase getSaveGameUseCase() {
        return getGameService();
    }

    public GetGameStatusUseCase getGetGameStatusUseCase() {
        return getGameService();
    }

    public GameFactory getGameFactory() {
        if (this.gameFactory == null) {
            this.gameFactory = new DefaultGameFactory();
        }
        return gameFactory;
    }

    public LoadGamePort getLoadGamePort() {
        return getGameRepository();
    }

    private GameRepository getGameRepository() {
        if (this.gameRepository == null) {
            this.gameRepository = new GameRepository();
        }
        return gameRepository;
    }

    public SaveGamePort getSaveGamePort() {
        return getGameRepository();
    }

    private GameService getGameService() {
        if (this.gameService == null) {
            this.gameService = new GameService(getEmmitGameEventPort(), getGameFactory(), getGameRepository(), getGameRepository());
        }
        return gameService;
    }

    public EmmitGameEventPort getEmmitGameEventPort() {
        if (this.emmitGameEventAdapter == null) {
            this.emmitGameEventAdapter = new EmmitGameEventAdapter(getEventPublisher());
        }
        return emmitGameEventAdapter;
    }

    public EventPublisher getEventPublisher() {
        if (this.eventPublisher == null) {
            this.eventPublisher = new EventPublisher();
        }
        return eventPublisher;
    }
}
