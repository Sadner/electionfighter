package pl.sadner.a1game.game.adapter.out;

import pl.sadner.a1game.game.application.port.out.LoadGamePort;
import pl.sadner.a1game.game.application.port.out.SaveGamePort;
import pl.sadner.a1game.game.domain.Game;
import pl.sadner.a1game.persistance.Repository;

public class GameRepository extends Repository<Game> implements LoadGamePort, SaveGamePort {
    @Override
    protected String getFileName() {
        return "game.txt";
    }
}
