package pl.sadner.a1game.game.application.port.in;

public interface LoadGameUseCase {
    void loadGame();
}
