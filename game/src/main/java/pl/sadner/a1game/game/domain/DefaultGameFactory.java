package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.config.GameConfig;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class DefaultGameFactory implements GameFactory {
    @Override
    public Game createGame() {
        var mapBoundary = new Boundary(1, 1, GameConfig.getLong("map.x"), GameConfig.getLong("map.y"));
        var enemies = getEnemies(mapBoundary);
        var map = new GameMap(mapBoundary, enemies);
        return new Game(map);
    }

    private List<Enemy> getEnemies(Boundary mapBoundary) {
        return LongStream.iterate(mapBoundary.getStartX(), x -> x + 2)
                .limit(mapBoundary.getEndX())
                .boxed()
                .mapToInt(Long::intValue)
                .mapToObj(x -> LongStream.iterate(mapBoundary.getStartY(), y -> y + 2)
                        .limit(mapBoundary.getEndY())
                        .boxed()
                        .mapToInt(Long::intValue)
                        .mapToObj(y -> new Enemy(x + y, Position.of(x, y)))
                ).flatMap(Function.identity()).collect(Collectors.toList());
    }
}
