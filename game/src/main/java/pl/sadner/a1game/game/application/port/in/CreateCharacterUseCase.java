package pl.sadner.a1game.game.application.port.in;

public interface CreateCharacterUseCase {
    void createCharacter(CreateCharacterCommand command);
}
