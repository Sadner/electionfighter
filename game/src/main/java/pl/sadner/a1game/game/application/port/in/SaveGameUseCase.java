package pl.sadner.a1game.game.application.port.in;

import pl.sadner.a1game.infrastructure.Result;

public interface SaveGameUseCase {
    Result<Void,Throwable> saveGame();
}
