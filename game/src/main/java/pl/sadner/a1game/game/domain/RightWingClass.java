package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.CharacterClass;
import pl.sadner.a1game.config.GameConfig;

public class RightWingClass extends CharacterClass {
    public RightWingClass() {
        super(GameConfig.getText("game.class.rightwing.name"), new TaxReductionAttack());
    }
}
