package pl.sadner.a1game.game.application.port.in;

import pl.sadner.a1game.game.domain.Direction;

public class MoveCommand {
    private final Direction direction;
    private final long characterId;

    public MoveCommand(Direction direction, long characterId) {
        this.direction = direction;
        this.characterId = characterId;
    }

    public Direction getDirection() {
        return direction;
    }

    public long getCharacterId() {
        return characterId;
    }
}
