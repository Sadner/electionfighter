package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.config.GameConfig;

class MoneyPromiseAttack extends Attack {
     MoneyPromiseAttack() {
        super(GameConfig.getText("attack.moneyPromise.name"), GameConfig.getLong("attack.moneyPromise.damage"));;
    }
}
