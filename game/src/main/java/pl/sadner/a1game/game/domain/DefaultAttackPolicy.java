package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.battle.domain.SimpleAttack;
import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.CharacterClass;

import java.util.List;

public class DefaultAttackPolicy implements AttackPolicy {

    @Override
    public List<Attack> getAvailableAttacks(CharacterClass characterClass) {
        return List.of(characterClass.getAttack(), new SimpleAttack());
    }
}
