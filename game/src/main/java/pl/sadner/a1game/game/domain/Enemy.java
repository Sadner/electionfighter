package pl.sadner.a1game.game.domain;

import java.io.Serializable;

 class Enemy implements Serializable {
    private final long id;
    private final Position position;


     Enemy(long id, Position position) {
        this.id = id;
        this.position = position;
    }

     long getId() {
        return id;
    }

     Position getPosition() {
        return position;
    }
}
