package pl.sadner.a1game.game.application.port.out;

import pl.sadner.a1game.infrastructure.eventbus.Event;

public interface EmmitGameEventPort {
    void publishEvent(Event event);
}
