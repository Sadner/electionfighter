package pl.sadner.a1game.game.application;

import pl.sadner.a1game.commons.CharacterClass;
import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.commons.Health;
import pl.sadner.a1game.config.GameConfig;
import pl.sadner.a1game.game.application.port.in.*;
import pl.sadner.a1game.game.application.port.out.EmmitGameEventPort;
import pl.sadner.a1game.game.application.port.out.LoadGamePort;
import pl.sadner.a1game.game.application.port.out.SaveGamePort;
import pl.sadner.a1game.game.domain.Character;
import pl.sadner.a1game.game.domain.*;
import pl.sadner.a1game.infrastructure.Result;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class GameService implements ExploreUseCase,
        GetGameStatusUseCase,
        CreateGameUseCase,
        CreateCharacterUseCase,
        LoadGameUseCase,
        SaveGameUseCase,
        GetAvailableClassesUseCase,
        AwardPlayerUseCase {
    private Game game;
    private final EmmitGameEventPort emmitGameEventPort;
    private final GameFactory gameFactory;
    private final LoadGamePort loadGamePort;
    private final SaveGamePort saveGamePort;

    public GameService(EmmitGameEventPort emmitGameEventPort,
                       GameFactory gameFactory,
                       LoadGamePort loadGamePort,
                       SaveGamePort saveGamePort) {
        this.emmitGameEventPort = emmitGameEventPort;
        this.gameFactory = gameFactory;
        this.loadGamePort = loadGamePort;
        this.saveGamePort = saveGamePort;
    }

    @Override
    public void explore() {
        Objects.requireNonNull(game);
        game.explore();
    }

    @Override
    public Result<Void, Throwable> move(MoveCommand command) {
        Objects.requireNonNull(game);
        try {
            game.move(new CharacterId(command.getCharacterId()), command.getDirection(), 1);
        } catch (EndOfMapException e) {
            return new Result<>(null, e);
        }
        game.getEmittedEvents()
                .forEach(emmitGameEventPort::publishEvent);
        game.getEmittedEvents().clear();

        return new Result<>(null, null);
    }

    @Override
    public Result<GameStatusDTO, Throwable> getGameStatus() {
        if (this.game != null) {
            return new Result<>(new GameStatusDTO(game.getState(), game.getCharacterInfo().getName(), game.getCharacterInfo().getExperience()), null);
        } else {
            return new Result<>(null, new GameNotExistsException());
        }
    }

    @Override
    public void createGame() {
        this.game = this.gameFactory.createGame();
    }

    @Override
    public void createCharacter(CreateCharacterCommand command) {
        var characterId = new CharacterId(ThreadLocalRandom.current().nextLong());
        var character = new Character(characterId,
                command.getName(),
                command.getCharacterClass(),
                Health.of(GameConfig.getLong("player.hp")),
                new DefaultAttackPolicy());
        this.game.setCharacter(character);
        this.game.resetCharacterPosition();
    }

    @Override
    public void loadGame() {
        loadGamePort.load().ifPresent(loadedGame -> this.game = loadedGame);
    }

    @Override
    public Result<Void, Throwable> saveGame() {
        if (this.game == null) {
            return new Result<>(null, new GameNotExistsException());
        }

        saveGamePort.save(this.game);
        return Result.empty();
    }

    @Override
    public List<CharacterClass> getAvailableClasses() {
        return List.of(new LeftWingClass(), new RightWingClass());
    }

    @Override
    public void awardPlayer(Experience experience) {
        game.awardPlayer(experience);
    }
}
