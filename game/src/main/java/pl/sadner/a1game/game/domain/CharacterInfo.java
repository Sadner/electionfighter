package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Experience;

public class CharacterInfo {
    private final String name;
    private final Experience experience;

    public CharacterInfo(String name, Experience experience) {
        this.name = name;
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public Experience getExperience() {
        return experience;
    }
}
