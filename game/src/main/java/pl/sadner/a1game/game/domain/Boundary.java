package pl.sadner.a1game.game.domain;

import java.io.Serializable;

class Boundary implements Serializable {
    private final long startX;
    private final long startY;
    private final long endX;
    private final long endY;

    Boundary(long startX, long startY, long endX, long endY) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
    }

    long getStartX() {
        return startX;
    }

    long getStartY() {
        return startY;
    }

    long getEndX() {
        return endX;
    }

    long getEndY() {
        return endY;
    }

    boolean contains(Position position) {
        return position.getX() >= startX && position.getX() <= endX &&
                position.getY() >= startY && position.getY() <= endY;
    }
}
