package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.config.GameConfig;

class TaxReductionAttack extends Attack {
    TaxReductionAttack() {
        super(GameConfig.getText("attack.taxReduction.name"), GameConfig.getLong("attack.taxReduction.damage"));;
    }
}
