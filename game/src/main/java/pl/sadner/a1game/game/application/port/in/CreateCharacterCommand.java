package pl.sadner.a1game.game.application.port.in;

import pl.sadner.a1game.commons.CharacterClass;

public class CreateCharacterCommand {
    private final String name;
    private final CharacterClass characterClass;

    public CreateCharacterCommand(String name, CharacterClass characterClass) {
        this.name = name;
        this.characterClass = characterClass;
    }

    public String getName() {
        return name;
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }
}
