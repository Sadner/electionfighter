package pl.sadner.a1game.game.application.port.in;

import pl.sadner.a1game.commons.Experience;

public interface AwardPlayerUseCase {
    void awardPlayer(Experience experience);
}
