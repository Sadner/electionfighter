package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.CharacterClass;
import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.commons.Health;

import java.io.Serializable;
import java.util.List;

public class Character implements Serializable {
    private final CharacterId id;
    private final String name;
    private final CharacterClass characterClass;
    private Position position;
    private final Health health;
    private final AttackPolicy attackPolicy;
    private Experience experience;

    public Character(CharacterId id, String name, CharacterClass characterClass, Health health, AttackPolicy attackPolicy) {
        this.id = id;
        this.name = name;
        this.characterClass = characterClass;
        this.health = health;
        this.attackPolicy = attackPolicy;
        this.experience = new Experience(0);
    }

    CharacterId getId() {
        return id;
    }

    String getName() {
        return name;
    }

    Position getPosition() {
        return position;
    }

    Health getHealth() {
        return health;
    }

    void moveTo(Position position) {
        this.position = position;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    List<Attack> getAvailableAttacks() {
        return attackPolicy.getAvailableAttacks(characterClass);
    }
}
