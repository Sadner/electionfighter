package pl.sadner.a1game.game.domain;

public enum GameState {
    WAITING_FOR_MOVE, BATTLE_IN_PROGRESS
}
