package pl.sadner.a1game.game.domain;

public interface GameFactory {

    Game createGame();
}
