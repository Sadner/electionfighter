package pl.sadner.a1game.game.adapter.out;

import pl.sadner.a1game.game.application.port.out.EmmitGameEventPort;
import pl.sadner.a1game.infrastructure.eventbus.Event;
import pl.sadner.a1game.infrastructure.eventbus.EventPublisher;
import pl.sadner.a1game.infrastructure.eventbus.Topic;

public class EmmitGameEventAdapter implements EmmitGameEventPort {

    private final EventPublisher eventPublisher;

    public EmmitGameEventAdapter(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void publishEvent(Event event) {
        eventPublisher.publish(Topic.of("pl/sadner/a1game/game"), event);
    }
}
