package pl.sadner.a1game.game.application.port.out;

import pl.sadner.a1game.game.domain.Game;

import java.util.Optional;

public interface LoadGamePort {
    Optional<Game> load();
}
