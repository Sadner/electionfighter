package pl.sadner.a1game.game.adapter.in;

import pl.sadner.a1game.battle.domain.BattleWonEvent;
import pl.sadner.a1game.game.application.port.in.ExploreUseCase;
import pl.sadner.a1game.game.application.port.in.AwardPlayerUseCase;
import pl.sadner.a1game.infrastructure.eventbus.Event;
import pl.sadner.a1game.infrastructure.eventbus.EventConsumer;
import pl.sadner.a1game.infrastructure.eventbus.Topic;

public class BattleEventHandler extends EventConsumer {

    private final ExploreUseCase exploreUseCase;
    private final AwardPlayerUseCase awardPlayerUseCase;

    public BattleEventHandler(ExploreUseCase exploreUseCase, AwardPlayerUseCase awardPlayerUseCase) {
        this.exploreUseCase = exploreUseCase;
        this.awardPlayerUseCase = awardPlayerUseCase;
        subscribe(Topic.of("pl/sadner/a1game/battle"));
    }

    @Override
    public void handle(Event event) {
      if(!(event instanceof BattleWonEvent))
          return;
      awardPlayerUseCase.awardPlayer(((BattleWonEvent) event).getExperience());
      exploreUseCase.explore();
    }
}
