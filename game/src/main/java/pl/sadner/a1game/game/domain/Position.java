package pl.sadner.a1game.game.domain;

import java.io.Serializable;

final class Position implements Serializable {
    private final int x;
    private final int y;

    Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int getX() {
        return x;
    }

     int getY() {
        return y;
    }

    static Position of(int x, int y) {
        return new Position(x, y);
    }

    Position withX(int value) {
        return Position.of(value, y);
    }

    Position withY(int value) {
        return Position.of(x, value);
    }

    Position move(Direction direction, int distance) {
        return switch (direction) {
            case NORTH -> this.withY(this.y - distance);
            case EAST -> this.withX(this.x + distance);
            case SOUTH -> this.withY(this.y + distance);
            case WEST -> this.withX(this.x - distance);
        };
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + x;
        hash = 31 * hash + y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Position)) return false;
        Position position = (Position) obj;
        return position.x == this.x && position.y == this.y;
    }
}
