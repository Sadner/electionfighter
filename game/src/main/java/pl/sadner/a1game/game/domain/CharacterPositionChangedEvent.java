package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.infrastructure.eventbus.Event;

public class CharacterPositionChangedEvent extends Event {
    private final CharacterId characterId;
    private final int newX;
    private final int newY;

    public CharacterPositionChangedEvent(CharacterId characterId, int newX, int newY) {
        super();
        this.newX = newX;
        this.newY = newY;
        this.characterId = characterId;
    }

    public int getNewX() {
        return newX;
    }

    public int getNewY() {
        return newY;
    }
}
