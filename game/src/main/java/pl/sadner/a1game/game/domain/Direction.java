package pl.sadner.a1game.game.domain;

public enum Direction {
    NORTH, EAST, SOUTH, WEST
}
