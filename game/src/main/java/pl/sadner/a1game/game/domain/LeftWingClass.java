package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.CharacterClass;
import pl.sadner.a1game.config.GameConfig;

 public class LeftWingClass extends CharacterClass {
     public LeftWingClass() {
        super(GameConfig.getText("game.class.leftwing.name"), new MoneyPromiseAttack());
    }
}
