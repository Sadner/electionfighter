package pl.sadner.a1game.game.application.port.in;

import pl.sadner.a1game.commons.CharacterClass;

import java.util.List;

public interface GetAvailableClassesUseCase {
    List<CharacterClass> getAvailableClasses();
}
