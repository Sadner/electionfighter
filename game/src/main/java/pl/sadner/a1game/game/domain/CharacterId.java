package pl.sadner.a1game.game.domain;

import java.io.Serializable;

 public class CharacterId implements Serializable {
    private final long id;

     public CharacterId(long id) {
        this.id = id;
    }

     long getId() {
        return id;
    }
}
