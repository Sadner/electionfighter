package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.CharacterClass;

import java.io.Serializable;
import java.util.List;

 interface AttackPolicy extends Serializable {
    List<Attack> getAvailableAttacks(CharacterClass characterClass);
}
