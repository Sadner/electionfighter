package pl.sadner.a1game.game.application.port.in;

import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.game.domain.GameState;

public class GameStatusDTO {
    private final GameState state;
    private final String characterName;
    private final Experience characterExperience;

    public GameStatusDTO(GameState state, String characterName, Experience characterExperience) {
        this.state = state;
        this.characterName = characterName;
        this.characterExperience = characterExperience;
    }

    public GameState getState() {
        return state;
    }


    public String getCharacterName() {
        return characterName;
    }

    public Experience getCharacterExperience() {
        return characterExperience;
    }
}
