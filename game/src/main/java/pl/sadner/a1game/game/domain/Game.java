package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.infrastructure.eventbus.Event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Game implements Serializable {
    private final GameMap map;
    private Character character;
    private final List<Event> emittedEvents = new ArrayList<>();
    private GameState state;

    public Game(GameMap map) {
        this.map = map;
        this.state = GameState.WAITING_FOR_MOVE;
    }

    public GameState getState() {
        return state;
    }

    public CharacterInfo getCharacterInfo() {
        return new CharacterInfo(character.getName(), character.getExperience());
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public void move(CharacterId id, Direction direction, int distance) throws EndOfMapException {
        Objects.requireNonNull(character);

        if (this.state != GameState.WAITING_FOR_MOVE)
            throw new IllegalStateException();

        var newPosition = character.getPosition().move(direction, distance);

        if (!map.getBoundary().contains(newPosition)) throw new EndOfMapException();
        else character.moveTo(newPosition);

        emittedEvents.add(new CharacterPositionChangedEvent(id, newPosition.getX(), newPosition.getY()));
        map.findEnemyAtPosition(newPosition)
                .ifPresent(enemy -> {
                    emittedEvents.add(new EnemyEncounteredEvent(id.getId(), enemy.getId(), character.getHealth(), character.getAvailableAttacks()));
                    this.state = GameState.BATTLE_IN_PROGRESS;
                });
    }

    public void awardPlayer(Experience experience) {
        character.setExperience(new Experience(character.getExperience().getValue() + experience.getValue()));
    }

    public void explore() {
        this.state = GameState.WAITING_FOR_MOVE;
    }

    public List<? extends Event> getEmittedEvents() {
        return emittedEvents;
    }

    public void resetCharacterPosition() {
        character.moveTo(Position.of((int) map.getBoundary().getStartX(), (int) map.getBoundary().getStartY()));
    }
}

