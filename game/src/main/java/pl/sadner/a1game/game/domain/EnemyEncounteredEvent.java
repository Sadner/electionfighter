package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Health;
import pl.sadner.a1game.infrastructure.eventbus.Event;

import java.util.List;

public class EnemyEncounteredEvent extends Event {
    private final long characterId;
    private final long enemyId;
    private final Health playerHealth;
    private final List<Attack> availableAttacks;

    public EnemyEncounteredEvent(long characterId, long enemyId, Health playerHealth, List<Attack> availableAttacks) {
        this.characterId = characterId;
        this.enemyId = enemyId;
        this.playerHealth = playerHealth;
        this.availableAttacks = availableAttacks;
    }

    public long getCharacterId() {
        return characterId;
    }

    public long getEnemyId() {
        return enemyId;
    }

    public Health getPlayerHealth() {
        return playerHealth;
    }

    public List<Attack> getAvailableAttacks() {
        return availableAttacks;
    }
}
