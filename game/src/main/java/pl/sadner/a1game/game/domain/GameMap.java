package pl.sadner.a1game.game.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

class GameMap implements Serializable {
    private final Boundary boundary;
    private final List<Enemy> enemies;

    GameMap(Boundary boundary, List<Enemy> enemies) {
        this.boundary = boundary;
        this.enemies = enemies;
    }

     Boundary getBoundary() {
        return boundary;
    }

    Optional<Enemy> findEnemyAtPosition(Position position) {
        return enemies.stream().filter(enemy -> enemy.getPosition().equals(position)).findFirst();
    }

}
