package pl.sadner.a1game.game.application.port.out;

import pl.sadner.a1game.game.domain.Game;

public interface SaveGamePort {
    void save(Game game);
}
