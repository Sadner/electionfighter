package pl.sadner.a1game.battle.domain;


import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import pl.sadner.a1game.commons.Attack;
import pl.sadner.a1game.commons.Health;

import java.util.List;


class BattleTest {

    @Nested
    class after_performed_attack {

        @Test
        void enemy_health_should_be_reduced_by_attack_damage() {
            //given
            var playerAttack = new Attack("test", 10L);
            var enemyHealth = 20L;
            var battle = new Battle(
                    new CharacterStats(1L, Health.of(10L), List.of(playerAttack)),
                    new EnemyInfo(1L, Health.of(enemyHealth), new SimpleAttack()),
                    new BattleProgress()
            );
            //when
            battle.performAttack(playerAttack);
            //when
            Assertions.assertEquals(enemyHealth - playerAttack.getBaseDamage(), battle.getBattleStatus().getEnemyHealth().getValue());
        }

        @Test
        void player_hp_should_be_reduced_by_enemy_attack_damage() {
            //given
            var enemyAttack = new Attack("test", 10L);
            var playerHealth = 20L;

            var battle = new Battle(
                    new CharacterStats(1L, Health.of(playerHealth), List.of()),
                    new EnemyInfo(1L, Health.of(30L), enemyAttack),
                    new BattleProgress()
            );
            //when
            battle.performAttack(new SimpleAttack());
            //when
            Assertions.assertEquals(playerHealth - enemyAttack.getBaseDamage(), battle.getBattleStatus().getPlayerHealth().getValue());
        }

        @Test
        void if_enemy_hp_is_lt_zero_battle_is_won() {
            //given
            var playerAttack = new Attack("test", 60L);
            var enemyHealth = 20L;
            var battle = new Battle(
                    new CharacterStats(1L, Health.of(10L), List.of(playerAttack)),
                    new EnemyInfo(1L, Health.of(enemyHealth), new SimpleAttack()),
                    new BattleProgress()
            );
            //when
            battle.performAttack(playerAttack);
            //when
            var events = battle.getEmittedEvents();
            MatcherAssert.assertThat(events, Matchers.contains(Matchers.instanceOf(BattleWonEvent.class)));
        }

    }

}