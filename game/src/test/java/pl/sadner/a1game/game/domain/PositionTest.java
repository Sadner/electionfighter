package pl.sadner.a1game.game.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PositionTest {

    Stream<Arguments> getDirections() {
        return Stream.of(
                Arguments.of(Direction.EAST, Coordinate.X, Sign.PLUS),
                Arguments.of(Direction.WEST, Coordinate.X, Sign.MINUS),
                Arguments.of(Direction.SOUTH, Coordinate.Y, Sign.PLUS),
                Arguments.of(Direction.NORTH, Coordinate.Y, Sign.MINUS)
        );
    }

    @ParameterizedTest(name = "When moving {0} {1} should be {1} {2} distance")
    @MethodSource("getDirections")
    void position_should_be_moved_in_correct_direction_by_distance(Direction direction, Coordinate coordinate, Sign sign) {
        var initValue = 10;
        var position = Position.of(initValue, initValue);
        var distance = 1;
        var changedPosition = position.move(direction, distance);

        var axisValue = switch (coordinate) {
            case X -> changedPosition.getX();
            case Y -> changedPosition.getY();
        };

        switch (sign) {
            case PLUS -> Assertions.assertEquals(initValue + distance, axisValue);
            case MINUS -> Assertions.assertEquals(initValue - distance, axisValue);
        }
    }

}

enum Coordinate {
    X, Y
}

enum Sign {
    PLUS, MINUS
}