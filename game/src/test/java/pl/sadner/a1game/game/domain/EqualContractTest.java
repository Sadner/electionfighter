package pl.sadner.a1game.game.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;
import pl.sadner.a1game.game.domain.Position;


class EqualContractTest {

    @Test
    public void positionEqualsContract() {
        EqualsVerifier.forClass(Position.class).verify();
    }

}