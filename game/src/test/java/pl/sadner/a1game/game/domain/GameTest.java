package pl.sadner.a1game.game.domain;

import pl.sadner.a1game.commons.Experience;
import pl.sadner.a1game.commons.Health;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.sadner.a1game.game.domain.*;
import pl.sadner.a1game.game.domain.Character;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class GameTest {
    @Nested
    class initial_value_of {

        @Test
        void character_experience_should_be_zero() {
            var game = new DefaultGameFactory().createGame();
            var character = new pl.sadner.a1game.game.domain.Character(new CharacterId(1L), "name", new RightWingClass(), Health.of(10L), new DefaultAttackPolicy());
            game.setCharacter(character);

            var initialCharacterExperience = game.getCharacterInfo().getExperience().getValue();

            assertEquals(0L, initialCharacterExperience);
        }

        @Test
        void position_should_be_start_map_boundary() {
            var startX = 1;
            var startY = 1;
            var mapBoundary = new Boundary(startX, startY, 2, 2);
            var enemies = new ArrayList<Enemy>();
            var map = new GameMap(mapBoundary, enemies);
            var game = new Game(map);
            var character = new pl.sadner.a1game.game.domain.Character(new CharacterId(1L), "name", new RightWingClass(), Health.of(10L), new DefaultAttackPolicy());
            game.setCharacter(character);

            game.resetCharacterPosition();

            assertEquals(startX, character.getPosition().getX());
            assertEquals(startY, character.getPosition().getY());
        }
    }


    @Test
    void when_layer_is_awarded_with_experience_experience_should_be_added() {
        var game = new DefaultGameFactory().createGame();
        var character = new pl.sadner.a1game.game.domain.Character(new CharacterId(1L), "name", new RightWingClass(), Health.of(10L), new DefaultAttackPolicy());
        game.setCharacter(character);
        var initialCharacterExperience = game.getCharacterInfo().getExperience().getValue();
        var experienceAward = 10L;

        game.awardPlayer(new Experience(experienceAward));

        var newCharacterExperience = game.getCharacterInfo().getExperience().getValue();
        assertEquals(initialCharacterExperience + experienceAward, newCharacterExperience);
    }

    @Nested
    class after_player_move {

        @Test
        void position_should_be_changed() throws EndOfMapException {
            var game = new DefaultGameFactory().createGame();
            var character = Mockito.mock(pl.sadner.a1game.game.domain.Character.class);
            when(character.getPosition()).thenReturn(Position.of(1, 1));
            game.setCharacter(character);

            game.move(new CharacterId(1L), Direction.EAST, 1);

            Mockito.verify(character).moveTo(any());
        }

        @Test
        void out_of_game_borders_exception_is_thrown() {
            var mapBoundary = new Boundary(1, 1, 2, 2);
            var enemies = new ArrayList<Enemy>();
            var map = new GameMap(mapBoundary, enemies);
            var game = new Game(map);
            var character = new pl.sadner.a1game.game.domain.Character(new CharacterId(1L), "name", new RightWingClass(), Health.of(10L), new DefaultAttackPolicy());
            game.setCharacter(character);
            game.resetCharacterPosition();

            assertThrows(EndOfMapException.class, () -> game.move(new CharacterId(1L), Direction.WEST, 5));
        }

        @Test
        void to_enemy_position_enemy_encountered_event_is_emitted() throws EndOfMapException {
            var mapBoundary = new Boundary(1, 1, 2, 2);
            var enemyPosition = Position.of(1, 1).move(Direction.EAST, 1);
            var character = new pl.sadner.a1game.game.domain.Character(new CharacterId(1L), "name", new RightWingClass(), Health.of(10L), new DefaultAttackPolicy());
            var enemies = List.of(new Enemy(1L, enemyPosition));
            var map = new GameMap(mapBoundary, enemies);
            var game = new Game(map);
            game.setCharacter(character);
            game.resetCharacterPosition();

            game.move(new CharacterId(1L), Direction.EAST, 1);

            var emittedEvents = game.getEmittedEvents();
            assertThat(emittedEvents, hasItem(Matchers.instanceOf(EnemyEncounteredEvent.class)));
        }

    }

    @Test
    void move_should_not_be_possible_when_in_battle() throws EndOfMapException {
        var game = prepareGameWithBattleMode();
        assertThrows(IllegalStateException.class, () ->
                game.move(new CharacterId(1L), Direction.EAST, 1));
    }

    @Test
    void enter_exploration_mode_should_change_game_state() throws EndOfMapException {
        var game = prepareGameWithBattleMode();

        game.explore();

        assertEquals(GameState.WAITING_FOR_MOVE, game.getState());
    }

    private Game prepareGameWithBattleMode() throws EndOfMapException {
        var mapBoundary = new Boundary(1, 1, 2, 2);
        var enemyPosition = Position.of(1, 1).move(Direction.EAST, 1);
        var character = new Character(new CharacterId(1L), "name", new RightWingClass(), Health.of(10L), new DefaultAttackPolicy());
        var enemies = List.of(new Enemy(1L, enemyPosition));
        var map = new GameMap(mapBoundary, enemies);
        var game = new Game(map);
        game.setCharacter(character);
        game.resetCharacterPosition();
        game.move(new CharacterId(1L), Direction.EAST, 1);
        return game;
    }

}