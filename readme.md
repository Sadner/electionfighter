# Challange
This simple game is a recruitment puzzle written for some company.
The challange here is to write a game for console using plain java.
Libraries and frameworks can only by used for build/test.

Maybe you will find my code good enough to me to join your project.

## Election Fighter
Election fighter is a simple game built for command line.
As a player you have to explore the map and crush your opponents.

## Controls
Just type command to choose menu option. 

## Build & Run
If you want to run game:

`gradlew run`

or build fat Jar with:

`gradlew shadowJar`

and run with:

`java -jar A1Game-1.0-SNAPSHOT-all.jar`

## Code coverage
If you want to generate code coverage report:

`gradlew codeCoverageReport`

(Please, don't do that. It's pretty lame. It's just demo)

## Configuration
You can find configuration properties in `config.properties` file where some 
basic configuration (messages, attack name, hp values etc.)

E.g. to change game title replace value of property

`game.name=Election Fighter`

