package pl.sadner.a1game.infrastructure.eventbus;

public interface Subscriber {
    void subscribe(Topic topic);
    void handle(Event event);
}
