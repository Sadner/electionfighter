package pl.sadner.a1game.infrastructure.eventbus;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class EventBus {
    private static EventBus instance = null;

    private EventBus() {
    }

    private final ConcurrentHashMap<Topic, List<Subscriber>> subscribers = new ConcurrentHashMap<>();

    public void publish(Topic topic, Event event) {
        subscribers.entrySet().stream()
                .filter(subscribedTopic -> subscribedTopic.getKey().equals(topic))
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .forEach(subscriber -> subscriber.handle(event));
    }

    public void subscribe(Topic topic, Subscriber subscriber) {
        Optional.ofNullable(subscribers.get(topic))
                .ifPresentOrElse(existingSubscribers -> {
                            existingSubscribers.add(subscriber);
                            subscribers.put(topic, existingSubscribers);
                        },
                        () -> {
                    var list = new ArrayList<Subscriber>();
                    list.add(subscriber);
                    subscribers.put(topic, list);
                });
    }

    public static EventBus getInstance() {
        if (instance == null) {
            synchronized (EventBus.class) {
                if (instance == null) {
                    instance = new EventBus();
                }
            }
        }
        return instance;
    }
}
