package pl.sadner.a1game.infrastructure;

import java.util.function.Consumer;

public class Result <S, F extends Throwable> {
    private final S success;
    private final F failure;

    public Result(S success, F failure) {
        this.success = success;
        this.failure = failure;
    }

    public static Result<Void, Throwable> empty() {
        return new Result<>(null, null);
    }

    public void fold(Consumer<S> onSuccess, Consumer<F> onFailure) {
        if(isFailure()) {
            onFailure.accept(failure);
        } else {
            onSuccess.accept(success);
        }
    }

    public S getOrThrow() throws Throwable {
        if(success != null) {
            return success;
        } else throw failure;
    }

    public void ifPresentOrElse(Consumer<? super S> action, Runnable emptyAction) {
        if(isFailure()) {
            emptyAction.run();
        } else {
            action.accept(success);
        }
    }

    public void ifSuccess(Consumer<? super S> action) {
        if(!isFailure()) {
            action.accept(success);
        }
    }

    public boolean isFailure() {
        return failure != null;
    }
}
