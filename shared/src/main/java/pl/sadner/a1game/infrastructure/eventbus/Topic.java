package pl.sadner.a1game.infrastructure.eventbus;


import java.util.Objects;

public final class Topic {
    private final String name;

    public Topic(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Topic of(String name) {
        return new Topic(name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Topic)) return false;
        if (this.name != null && ((Topic) obj).getName() != null) {
            return this.name.equals(((Topic) obj).getName());
        } else return this.name == null && ((Topic) obj).getName() == null;
    }
}
