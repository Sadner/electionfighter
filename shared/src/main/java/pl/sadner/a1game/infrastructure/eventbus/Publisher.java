package pl.sadner.a1game.infrastructure.eventbus;

public interface Publisher {
    void publish(Topic topic, Event event);
}
