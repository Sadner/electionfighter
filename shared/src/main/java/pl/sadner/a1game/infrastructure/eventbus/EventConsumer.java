package pl.sadner.a1game.infrastructure.eventbus;

public abstract class EventConsumer implements Subscriber {
    @Override
    public void subscribe(Topic topic) {
        EventBus.getInstance().subscribe(topic,this);
    }
}
