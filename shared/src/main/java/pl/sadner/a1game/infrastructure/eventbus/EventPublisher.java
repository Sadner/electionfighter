package pl.sadner.a1game.infrastructure.eventbus;

public class EventPublisher implements Publisher {

    @Override
    public void publish(Topic topic, Event event) {
        EventBus.getInstance().publish(topic, event);
    }
}
