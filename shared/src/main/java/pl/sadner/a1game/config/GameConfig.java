package pl.sadner.a1game.config;

import java.util.ResourceBundle;

public class GameConfig {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    public static String getText(String key) {
        return resourceBundle.getString(key);
    }
    public static long getLong(String key) {
        return Long.parseLong(resourceBundle.getString(key));
    }
    public static int getInt(String key) {
        return Integer.parseInt(resourceBundle.getString(key));
    }
}
