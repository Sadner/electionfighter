package pl.sadner.a1game.persistance;

import java.io.*;
import java.util.Optional;

public abstract class Repository<T> {
    protected abstract String getFileName();

    public Optional<T> load() {
        T entity = null;
        var file = new File(getFileName());

        try (var objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            var object = objectInputStream.readObject();
            if(object != null) {
                entity = (T) object;
            }
        } catch (IOException | ClassNotFoundException e) {
           return Optional.ofNullable(entity);
        }
        return Optional.ofNullable(entity);
    }

    public void save(T entity) {
        var file = new File(getFileName());
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (var objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOutputStream.writeObject(entity);
            objectOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
