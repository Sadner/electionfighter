package pl.sadner.a1game.commons;

import java.io.Serializable;

public class Experience implements Serializable {
    private final long value;

    public Experience(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
