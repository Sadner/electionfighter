package pl.sadner.a1game.commons;

import java.io.Serializable;

public class CharacterClass implements Serializable {
    private final String name;
    private final Attack attack;

    public CharacterClass(String name, Attack attack) {
        this.name = name;
        this.attack = attack;
    }

    public String getName() {
        return name;
    }

    public Attack getAttack() {
        return attack;
    }

    public static CharacterClass of(String name, Attack attack) {
        return new CharacterClass(name, attack);
    }
}
