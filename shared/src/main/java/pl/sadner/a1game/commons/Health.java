package pl.sadner.a1game.commons;

import java.io.Serializable;

public class Health implements Serializable {
    private final long value;

    public Health(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public static Health of(long value) {
        return new Health(value);
    }
}
