package pl.sadner.a1game.commons;

public class Millisecond {
    private final long value;

    public Millisecond(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
