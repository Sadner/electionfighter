package pl.sadner.a1game.commons;

public class Attack implements Action {
    private final String name;
    private final long baseDamage;

    public Attack(String name, long baseDamage) {
        this.name = name;
        this.baseDamage = baseDamage;
    }

    public String getName() {
        return name;
    }

    public long getBaseDamage() {
        return baseDamage;
    }
}
