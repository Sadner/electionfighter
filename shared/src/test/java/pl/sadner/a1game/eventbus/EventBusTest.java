package pl.sadner.a1game.eventbus;

import pl.sadner.a1game.infrastructure.eventbus.Event;
import pl.sadner.a1game.infrastructure.eventbus.EventConsumer;
import pl.sadner.a1game.infrastructure.eventbus.EventPublisher;
import pl.sadner.a1game.infrastructure.eventbus.Topic;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EventBusTest {

    @Test
    void event_should_be_delivered_for_all_subscribers_of_topic() {
        var subscriber = mockEventConsumer();
        var otherSubscriber = mockEventConsumer();
        var eventTopic = Topic.of("eventtopic");
        subscriber.subscribe(eventTopic);
        otherSubscriber.subscribe(eventTopic);
        var publisher = new EventPublisher();
        var event = new Event();

        publisher.publish(eventTopic, event);

        verify(subscriber).handle(event);
        verify(otherSubscriber).handle(event);
    }

    @Test
    void event_should_not_be_delivered_when_subscribed_for_other_topic() {
        var subscriber = mockEventConsumer();
        var subscribedTopic = Topic.of("subtopic");
        var eventTopic = Topic.of("eventtopic");
        subscriber.subscribe(subscribedTopic);

        var publisher = new EventPublisher();
        var event = new Event();

        publisher.publish(eventTopic, event);

        verify(subscriber, never()).handle(any());
    }

    private EventConsumer mockEventConsumer() {
        var subscriber = Mockito.mock(EventConsumer.class);
        doCallRealMethod().when(subscriber).subscribe(any());
        return subscriber;
    }
}
