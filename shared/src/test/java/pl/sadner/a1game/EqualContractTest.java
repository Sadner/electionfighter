package pl.sadner.a1game;

import pl.sadner.a1game.infrastructure.eventbus.Topic;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;


class EqualContractTest {

    @Test
    public void topicEqualsContract() {
        EqualsVerifier.forClass(Topic.class).verify();
    }

}